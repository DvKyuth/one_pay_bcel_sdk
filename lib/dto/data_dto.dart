import 'package:one_pay_bcel_sdk/generated/json/base/json_field.dart';
import 'package:one_pay_bcel_sdk/generated/json/data_dto.g.dart';

@JsonSerializable()
class DataDto {
  late String code;
  late String value;

  DataDto();

  factory DataDto.fromJson(Map<String, dynamic> json) => $DataDtoFromJson(json);

  Map<String, dynamic> toJson() => $DataDtoToJson(this);

  @override
  String toString() {
    final result = "$code${value.length.toString().padLeft(2, "0")}$value";
    return result;
  }
}
