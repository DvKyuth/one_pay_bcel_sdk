import 'dart:convert';

import 'package:one_pay_bcel_sdk/generated/json/base/json_field.dart';
import 'package:one_pay_bcel_sdk/generated/json/po_store_dto.g.dart';

@JsonSerializable()
class PoStoreDto {
  String? mcid;
  String? country;
  String? province;
  String? subscribeKey;
  String? terminalid;

  PoStoreDto();

  factory PoStoreDto.fromJson(Map<String, dynamic> json) => $PoStoreDtoFromJson(json);

  Map<String, dynamic> toJson() => $PoStoreDtoToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
