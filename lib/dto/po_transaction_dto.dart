import 'dart:convert';

import 'package:one_pay_bcel_sdk/generated/json/base/json_field.dart';
import 'package:one_pay_bcel_sdk/generated/json/po_transaction_dto.g.dart';
import 'package:uuid/uuid.dart';

@JsonSerializable()
class PoTransactionDto {
  String? invoiceId;
  String? amount;
  String? currency;
  String? description;
  String reference = Uuid().v1();

  PoTransactionDto();

  factory PoTransactionDto.fromJson(Map<String, dynamic> json) => $PoTransactionDtoFromJson(json);

  Map<String, dynamic> toJson() => $PoTransactionDtoToJson(this);

  @override
  String toString() {
    return jsonEncode(this);
  }
}
