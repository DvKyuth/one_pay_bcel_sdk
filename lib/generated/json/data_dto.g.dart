import 'package:one_pay_bcel_sdk/dto/data_dto.dart';
import 'package:one_pay_bcel_sdk/generated/json/base/json_convert_content.dart';

DataDto $DataDtoFromJson(Map<String, dynamic> json) {
  final DataDto dataDto = DataDto();
  final String? code = jsonConvert.convert<String>(json['code']);
  if (code != null) {
    dataDto.code = code;
  }
  final String? value = jsonConvert.convert<String>(json['value']);
  if (value != null) {
    dataDto.value = value;
  }
  return dataDto;
}

Map<String, dynamic> $DataDtoToJson(DataDto entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['code'] = entity.code;
  data['value'] = entity.value;
  return data;
}
