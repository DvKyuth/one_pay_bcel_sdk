import 'package:one_pay_bcel_sdk/dto/po_store_dto.dart';
import 'package:one_pay_bcel_sdk/generated/json/base/json_convert_content.dart';

PoStoreDto $PoStoreDtoFromJson(Map<String, dynamic> json) {
  final PoStoreDto poStoreDto = PoStoreDto();
  final String? mcid = jsonConvert.convert<String>(json['mcid']);
  if (mcid != null) {
    poStoreDto.mcid = mcid;
  }
  final String? country = jsonConvert.convert<String>(json['country']);
  if (country != null) {
    poStoreDto.country = country;
  }
  final String? province = jsonConvert.convert<String>(json['province']);
  if (province != null) {
    poStoreDto.province = province;
  }
  final String? subscribeKey = jsonConvert.convert<String>(json['subscribeKey']);
  if (subscribeKey != null) {
    poStoreDto.subscribeKey = subscribeKey;
  }
  final String? terminalid = jsonConvert.convert<String>(json['terminalid']);
  if (terminalid != null) {
    poStoreDto.terminalid = terminalid;
  }
  return poStoreDto;
}

Map<String, dynamic> $PoStoreDtoToJson(PoStoreDto entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['mcid'] = entity.mcid;
  data['country'] = entity.country;
  data['province'] = entity.province;
  data['subscribeKey'] = entity.subscribeKey;
  data['terminalid'] = entity.terminalid;
  return data;
}
