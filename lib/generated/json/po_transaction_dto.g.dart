import 'package:one_pay_bcel_sdk/dto/po_transaction_dto.dart';
import 'package:one_pay_bcel_sdk/generated/json/base/json_convert_content.dart';

PoTransactionDto $PoTransactionDtoFromJson(Map<String, dynamic> json) {
  final PoTransactionDto poTransactionDto = PoTransactionDto();
  final String? invoiceId = jsonConvert.convert<String>(json['invoiceId']);
  if (invoiceId != null) {
    poTransactionDto.invoiceId = invoiceId;
  }
  final String? amount = jsonConvert.convert<String>(json['amount']);
  if (amount != null) {
    poTransactionDto.amount = amount;
  }
  final String? currency = jsonConvert.convert<String>(json['currency']);
  if (currency != null) {
    poTransactionDto.currency = currency;
  }
  final String? description = jsonConvert.convert<String>(json['description']);
  if (description != null) {
    poTransactionDto.description = description;
  }
  final String? reference = jsonConvert.convert<String>(json['reference']);
  if (reference != null) {
    poTransactionDto.reference = reference;
  }
  return poTransactionDto;
}

Map<String, dynamic> $PoTransactionDtoToJson(PoTransactionDto entity) {
  final Map<String, dynamic> data = <String, dynamic>{};
  data['invoiceId'] = entity.invoiceId;
  data['amount'] = entity.amount;
  data['currency'] = entity.currency;
  data['description'] = entity.description;
  data['reference'] = entity.reference;
  return data;
}
