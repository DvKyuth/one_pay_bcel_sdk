import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:one_pay_bcel_sdk/dto/po_store_dto.dart';
import 'package:one_pay_bcel_sdk/dto/po_transaction_dto.dart';
import 'package:one_pay_bcel_sdk/qr_generator.dart';
import 'package:pubnub/pubnub.dart';

class PoManager extends ChangeNotifier {
  PoManager(
    this.mcId,
    this.country,
    this.province,
    this.subscribeKey,
    this.terminalId,
    this.uuid,
    this.inn,
    this.applicationId,
  );

  late String mcId;
  late String country;
  late String province;
  late String subscribeKey;
  late String terminalId;
  late String uuid;
  late String inn;
  late String applicationId;

  Envelope? event;
  StreamSubscription? subscription;
  PubNub? pubNub;

  getDeepLinkUrl({String amount = "0", String currency = "418", String invoiceId = "", String description = ""}) {
    final PoStoreDto storeDto = PoStoreDto();
    storeDto
      ..mcid = mcId
      ..subscribeKey = subscribeKey
      ..country = country
      ..province = province
      ..terminalid = terminalId;

    final PoTransactionDto transactionDto = PoTransactionDto();
    transactionDto
      ..amount = amount
      ..currency = currency
      ..invoiceId = invoiceId
      ..description = description;

    final QrGenerator qrGenerator = QrGenerator(storeDto, transactionDto, inn, applicationId);

    uuid = transactionDto.reference;

    return qrGenerator.getDeepLinkUrl();
  }

  start() {
    final Keyset pubNubConfig = Keyset(
      subscribeKey: subscribeKey,
      uuid: UUID(uuid),
      publishKey: subscribeKey,
    );

    pubNub ??= PubNub(
      defaultKeyset: pubNubConfig,
    );

    final String channelName = "uuid-" + mcId + '-' + uuid;

    subscription?.cancel();
    subscription = pubNub?.subscribe(channels: {channelName}).messages.listen(
          (event) {
            this.event = event;
            notifyListeners();
            subscription?.cancel();
          },
        );
  }
}
